package org.glittum.jaorm.internal;

import java.time.LocalDateTime;
import java.util.List;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.annotation.Table;

@Table("news")
public class TestParentEntity {

    @Id
    @Column("id")
    private long id;

    @OneToMany(mappedBy = "parent")
    private List<TestChildEntity> child;

    @OneToOne(column = "child_id")
    private TestLonlyChildEntity lonlyChildEntity;

    TestParentEntity() {
    }

    public TestParentEntity(long id, LocalDateTime released, String title, String image, String text) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

