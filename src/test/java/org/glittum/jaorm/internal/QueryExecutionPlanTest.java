package org.glittum.jaorm.internal;


import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.glittum.jaorm.internal.EntityExtractionService.generateExecutionPlan;

import java.util.Iterator;
import java.util.Map;

import org.glittum.jaorm.criteria.NewsEntity;
import org.glittum.jaorm.criteria.Query;
import org.glittum.jaorm.criteria.QueryBuilder;
import org.junit.Test;

public class QueryExecutionPlanTest {

    @Test
    public void skal_generere_query_for_child_records() {
        QueryExecutionPlan<TestParentEntity> plan = new QueryExecutionPlan<>(TestParentEntity.class);
        plan.twoWayBindingGuard(plan.getEntity());
        Query<TestParentEntity> rootQuery = plan.getRootQuery();
        assertThat(rootQuery).isNotNull();

        assertThat(plan.requiresFetchForChildren()).isTrue();
        Map<ChildRecordClasses, QueryBuilder<?>> childRecordQuerries = plan.getChildRecordQuerries();
        assertThat(childRecordQuerries).isNotEmpty();
        assertThat(childRecordQuerries.keySet()).hasSize(2);

        Iterator<QueryBuilder<?>> iterator = childRecordQuerries.values().iterator();
        iterator.next();
        QueryBuilder<?> next = iterator.next();
        next.withParameterValue(TestLonlyChildEntity.class.getSimpleName(), 1L);
        QueryExecutionPlan<?> queryExecutionPlan = generateExecutionPlan(next, TestParentEntity.class);
        childRecordQuerries = queryExecutionPlan.getChildRecordQuerries();
        assertThat(childRecordQuerries).isEmpty();
    }

    @Test
    public void skal_ikke_generere_query_for_child_records_når_det_ikke_eksisterer_noen() {
        QueryExecutionPlan<NewsEntity> plan = new QueryExecutionPlan<>(NewsEntity.class);

        Query<NewsEntity> rootQuery = plan.getRootQuery();

        assertThat(rootQuery).isNotNull();

        assertThat(plan.requiresFetchForChildren()).isFalse();
        Map<ChildRecordClasses, QueryBuilder<?>> childRecordQuerries = plan.getChildRecordQuerries();
        assertThat(childRecordQuerries).isEmpty();
    }
}