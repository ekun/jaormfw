package org.glittum.jaorm.internal;

import java.time.LocalDateTime;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.annotation.Table;

@Table("news_2")
public class TestLonlyChildEntity {

    @Id
    @Column("id")
    private long id;

    @OneToOne(column = "parent_id")
    private TestParentEntity parent;

    TestLonlyChildEntity() {
    }

    public TestLonlyChildEntity(long id, LocalDateTime released, String title, String image, String text) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

