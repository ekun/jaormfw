package org.glittum.jaorm.internal;

import java.time.LocalDateTime;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.Table;

@Table("news")
public class TestChildEntity {

    @Id
    @Column("id")
    private long id;

    @ManyToOne(column = "parent_id")
    private TestParentEntity parent;

    TestChildEntity() {
    }

    public TestChildEntity(long id, LocalDateTime released, String title, String image, String text) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

