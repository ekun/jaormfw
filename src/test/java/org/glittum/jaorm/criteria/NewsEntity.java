package org.glittum.jaorm.criteria;

import java.time.LocalDateTime;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.Table;
import org.glittum.jaorm.annotation.Transient;

@Table("news")
public class NewsEntity {

    @Id
    @Column("id")
    private Long id;

    @Column("released")
    private LocalDateTime released;

    @Column("archived")
    private boolean archived = false;

    @Column("title")
    private String title;

    @Column("image")
    private String image;

    @Column("text")
    private String text;

    @Column("regres")
    private String regres;

    @Transient
    private NewsEntity next;
    @Transient
    private NewsEntity previous;

    NewsEntity() {
    }

    public NewsEntity(Long id, LocalDateTime released, String title, String image, String text) {
        this.id = id;
        this.released = released;
        this.title = title;
        this.image = image;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getReleased() {
        return released;
    }

    public void setReleased(LocalDateTime released) {
        this.released = released;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NewsEntity getNext() {
        return next;
    }

    public void setNext(NewsEntity next) {
        this.next = next;
    }

    public NewsEntity getPrevious() {
        return previous;
    }

    public void setPrevious(NewsEntity previous) {
        this.previous = previous;
    }

    public String getRegres() {
        return regres;
    }

    public void setRegres(String regres) {
        this.regres = regres;
    }

    public boolean isArchived() {
        return archived;
    }
}

