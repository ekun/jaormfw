package org.glittum.jaorm.criteria;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.Table;

@Table("news_comment")
public class TestChildEntity {

    @Id
    @Column("id")
    private Long id;

    @ManyToOne(column = "parent_id")
    private TestParentEntity parent;

    TestChildEntity() {
    }

    public TestChildEntity(Long id, TestParentEntity parent) {
        this.id = id;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

