package org.glittum.jaorm.criteria;

import java.time.LocalDateTime;
import java.util.List;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.annotation.Table;
import org.glittum.jaorm.internal.TestLonlyChildEntity;

@Table("news")
public class TestParentEntity {

    @Id
    @Column("id")
    private Long id;

    @OneToMany(mappedBy = "parent")
    private List<TestChildEntity> child;

    @OneToOne(column = "child_id")
    private TestLonlyChildEntity lonlyChildEntity;

    TestParentEntity() {
    }

    public TestParentEntity(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

