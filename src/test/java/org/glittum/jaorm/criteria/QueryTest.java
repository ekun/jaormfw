package org.glittum.jaorm.criteria;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.time.LocalDateTime;

import org.glittum.jaorm.exception.QueryInconsistencyException;
import org.junit.Test;

public class QueryTest {

    @Test
    public void should_create_an_default_query_builder_when_createing_for_an_entity_only() {
        Query<NewsEntity> typedQuery = EntityQueryBuilder.forClass(NewsEntity.class).build();

        assertThat(typedQuery.toStatement()).isEqualToIgnoringCase("SELECT archived, id, image, regres, released, text, title FROM news");
    }

    @Test
    public void should_add_limit_to_query_when_build_with_limit() {
        Query<NewsEntity> typedQuery = EntityQueryBuilder.forClass(NewsEntity.class).limit(1L).offset(50L).build();

        assertThat(typedQuery.toStatement()).isEqualToIgnoringCase("SELECT archived, id, image, regres, released, text, title FROM news OFFSET 50 LIMIT 1");
    }

    @Test
    public void should_add_where_clause_when_adding_filter() {
        Query typedQuery = EntityQueryBuilder.forClass(NewsEntity.class)
                .where(FilterCriteria.equals("title", "title"))
                .withParameterValue("title", "LOL TROLL")
                .build();

        assertThat(typedQuery.toStatement()).isEqualToIgnoringCase("SELECT archived, id, image, regres, released, text, title FROM news WHERE title = ?");
        assertThat(typedQuery.getParameters()).containsExactly("LOL TROLL");
    }

    @Test
    public void should_order_parameters_in_same_order_as_where_clause() {
        Query typedQuery = EntityQueryBuilder.forClass(NewsEntity.class)
                .where(FilterCriteria.equals("title", "title"))
                .where(FilterCriteria.equals("regres", "regres"))
                .withParameterValue("title", "LOL TROLL")
                .withParameterValue("regres", "ZNORK")
                .build();

        assertThat(typedQuery.toStatement()).isEqualToIgnoringCase("SELECT archived, id, image, regres, released, text, title FROM news WHERE regres = ? AND title = ?");
        assertThat(typedQuery.getParameters()).containsExactly("ZNORK", "LOL TROLL");
    }

    @Test
    public void should_order_parameters_in_same_order_as_where_clause_inside_and_critera() {
        Query typedQuery = EntityQueryBuilder.forClass(NewsEntity.class)
                .where(FilterCriteria.equals("title", "title").and(FilterCriteria.equals("regres", "regres")))
                .withParameterValue("title", "LOL TROLL")
                .withParameterValue("regres", "ZNORK")
                .build();

        assertThat(typedQuery.toStatement()).isEqualToIgnoringCase("SELECT archived, id, image, regres, released, text, title FROM news WHERE (title = ? AND regres = ?)");
        assertThat(typedQuery.getParameters()).containsExactly("LOL TROLL", "ZNORK");
    }

    @Test
    public void consistency_check_operation() {
        try {
            EntityQueryBuilder.forClass(NewsEntity.class).delete("test").select("title").build();
        } catch (QueryInconsistencyException e) {
            fail("Should clear operations when adding delete operation");
        }
    }

    @Test
    public void delete_operation_should_clear_operation_state_from_builder() {
        try {
            EntityQueryBuilder.forClass(NewsEntity.class).select("title").delete("test").build();
        } catch (QueryInconsistencyException e) {
            fail("Should clear operations when adding delete operation");
        }
    }

    @Test
    public void delete_operation_create_delete_query() {
        Query<NewsEntity> query = EntityQueryBuilder.forClass(NewsEntity.class).delete().build();

        assertThat(query.toStatement()).isEqualToIgnoringCase("DELETE FROM news");
    }

    @Test
    public void delete_operation_create_delete_query_with_filter() {
        Query<NewsEntity> query = EntityQueryBuilder.forClass(NewsEntity.class).delete()
                .where(FilterCriteria.equals("id", "id"))
                .withParameterValue("id", 1L)
                .build();

        assertThat(query.toStatement()).isEqualToIgnoringCase("DELETE FROM news WHERE id = ?");
        assertThat(query.getParameters()).containsExactly(1L);
    }

    @Test
    public void insert_operation_create_insert_query() {
        LocalDateTime now = LocalDateTime.now();
        NewsEntity news = new NewsEntity(null, now, "title", "image", "text");
        Query<NewsEntity> query = EntityQueryBuilder.forClass(NewsEntity.class).insert(news).build();

        assertThat(query.toStatement()).isEqualToIgnoringCase("INSERT INTO news (title, released, text, archived, image) VALUES (?, ?, ?, ?, ?) RETURNING id AS id");
        assertThat(query.getParameters()).containsExactly("title", now, "text", false, "image");
    }

    @Test
    public void insert_operation_create_insert_query_children() {
        LocalDateTime now = LocalDateTime.now();
        TestParentEntity news = new TestParentEntity(100L);
        TestChildEntity child = new TestChildEntity(null, news);
        Query<TestChildEntity> query = EntityQueryBuilder.forClass(TestChildEntity.class).insert(child).build();

        assertThat(query.toStatement()).isEqualToIgnoringCase("INSERT INTO news_comment (parent_id) VALUES (?) RETURNING id AS id");
        assertThat(query.getParameters()).containsExactly(100L);
    }
}