package org.glittum.jaorm.mapper;

import java.util.Objects;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;

public class FieldWithAlias implements Comparable {
    private final String table;
    private final String column;
    private final String alias;
    private final boolean nullable; // For inserts ?
    private final FieldType type;

    public FieldWithAlias(Column fieldAnnotation, String table) {
        this.table = table;
        this.column = fieldAnnotation.value();
        this.alias = null;
        this.nullable = fieldAnnotation.nullable();
        this.type = FieldType.COLUMN;
    }

    public FieldWithAlias(OneToMany fieldAnnotation, String table) {
        this.table = table;
        this.column = fieldAnnotation.mappedBy();
        this.alias = null;
        this.nullable = false;
        this.type = FieldType.ONE_TO_MANY;
    }

    public FieldWithAlias(ManyToOne fieldAnnotation, String table) {
        this.table = table;
        this.column = fieldAnnotation.column();
        this.alias = null;
        this.nullable = false;
        this.type = FieldType.MANY_TO_ONE;
    }

    public FieldWithAlias(OneToOne fieldAnnotation, String table) {
        this.table = table;
        this.column = fieldAnnotation.column();
        this.alias = null;
        this.nullable = false;
        this.type = FieldType.ONE_TO_ONE;
    }

    boolean hasAlias() {
        return alias != null && !alias.isEmpty();
    }

    String getTable() {
        return table;
    }

    String getColumn() {
        return column;
    }

    String getAlias() {
        return alias;
    }

    public boolean isColumn() {
        return FieldType.COLUMN.equals(type);
    }

    boolean isNullable() {
        return nullable;
    }

    public String getSelectString() {
        StringBuilder stringBuilder = new StringBuilder(column);
        if (hasAlias()) {
            stringBuilder.append(" AS ").append(alias);
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldWithAlias that = (FieldWithAlias) o;
        return table.equals(that.table) &&
                column.equals(that.column) &&
                Objects.equals(alias, that.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, column, alias);
    }

    @Override
    public int compareTo(Object o) {
        if (this == o) {
            return 0;
        }
        String other = ((FieldWithAlias) o).table + "." + ((FieldWithAlias) o).column;
        return (table + "." + column).compareToIgnoreCase(other);
    }

    @Override
    public String toString() {
        return "FieldWithAlias{" +
                "table='" + table + '\'' +
                ", column='" + column + '\'' +
                ", alias='" + alias + '\'' +
                ", nullable=" + nullable +
                '}';
    }
}
