package org.glittum.jaorm.mapper;

import org.glittum.jaorm.Database;

public class FunctionalMapper {
    private final Database.Row rs;
    private final FieldWithAlias value;

    public FunctionalMapper(Database.Row rs, FieldWithAlias value) {
        this.rs = rs;
        this.value = value;
    }

    public Database.Row rs() {
        return rs;
    }

    public FieldWithAlias value() {
        return value;
    }
}
