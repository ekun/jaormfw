package org.glittum.jaorm.mapper;

enum FieldType {
    COLUMN, MANY_TO_ONE, ONE_TO_MANY, ONE_TO_ONE
}
