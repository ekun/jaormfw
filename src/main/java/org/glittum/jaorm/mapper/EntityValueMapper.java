package org.glittum.jaorm.mapper;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.glittum.jaorm.Database;
import org.glittum.jaorm.exception.UnsupportedFieldTypeException;

public class EntityValueMapper {

    private final Map<Class<?>, Function<FunctionalMapper, Object>> MAPPER = new HashMap<>();

    public EntityValueMapper() {
        MAPPER.put(long.class, this::mapLong);
        MAPPER.put(int.class, this::mapInteger);
        MAPPER.put(boolean.class, this::mapBoolean);
        MAPPER.put(Long.class, this::mapLong);
        MAPPER.put(Integer.class, this::mapInteger);
        MAPPER.put(Boolean.class, this::mapBoolean);
        MAPPER.put(String.class, this::mapString);
        MAPPER.put(LocalDateTime.class, this::mapLocalDateTime);
        MAPPER.put(LocalDate.class, this::mapLocalDate);
        MAPPER.put(BigDecimal.class, this::mapBigDecimal);
        MAPPER.put(Clob.class, this::mapClob);
        MAPPER.put(Blob.class, this::mapBlob);
    }

    private Object mapBlob(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getBlob(mapper.value().getAlias());
            } else {
                return mapper.rs().getBlob(mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapClob(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getClob(mapper.value().getAlias());
            } else {
                return mapper.rs().getClob(mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapBigDecimal(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getBigdecimal(mapper.value().getAlias());
            } else {
                return mapper.rs().getBigdecimal(mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> Object extractValueForColumn(Map.Entry<Field, FieldWithAlias> entry, Database.Row rs, Class<T> entity) {
        Field key = entry.getKey();
        FieldWithAlias value = entry.getValue();
        Class<?> type = key.getType();
        FunctionalMapper fm = new FunctionalMapper(rs, value);
        Function<FunctionalMapper, Object> consumer = MAPPER.get(type);

        if (consumer != null) {
            return consumer.apply(fm);
        }

        throw new UnsupportedFieldTypeException(type, key.getName(), entity);
    }

    private Object mapString(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getString(mapper.value().getAlias());
            } else {
                return mapper.rs().getString(mapper.value().getTable(), mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapLong(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getLong(mapper.value().getTable(), mapper.value().getAlias());
            } else {
                return mapper.rs().getLong(mapper.value().getTable(), mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapInteger(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getInt(mapper.value().getAlias());
            } else {
                return mapper.rs().getInt(mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapBoolean(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                return mapper.rs().getBoolean(mapper.value().getTable(), mapper.value().getAlias());
            } else {
                return mapper.rs().getBoolean(mapper.value().getTable(), mapper.value().getColumn());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapLocalDate(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                Instant instant = mapper.rs().getInstant(mapper.value().getAlias());
                if (instant != null) {
                    return LocalDate.ofInstant(instant, ZoneId.systemDefault());
                }
            } else {
                Instant instant = mapper.rs().getInstant(mapper.value().getColumn());
                if (instant != null) {
                    return LocalDate.ofInstant(instant, ZoneId.systemDefault());
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object mapLocalDateTime(FunctionalMapper mapper) {
        try {
            if (mapper.value().hasAlias()) {
                Instant instant = mapper.rs().getInstant(mapper.value().getAlias());
                if (instant != null) {
                    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                }
            } else {
                Instant instant = mapper.rs().getInstant(mapper.value().getColumn());
                if (instant != null) {
                    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}