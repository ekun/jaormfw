package org.glittum.jaorm;

/**
 * Transaction handler
 *
 * Use {@link #in(Runnable)} for work in transaction.
 */
public class Transaction {
    private final Database database;

    public Transaction() {
        this.database = ((EntityManagerImpl)EntityManagerHolder.get()).getDatabase();
    }

    /**
     * Create a transaction for multiple database operations like
     * {@link EntityManager#persist(Object)}
     * {@link EntityManager#delete(Object)}
     *
     * @param operation is a functional interface to allow transaction to run in a
     *                  thread.
     */
    public void in(Runnable operation) {
        database.doInTransaction(operation);
    }
}