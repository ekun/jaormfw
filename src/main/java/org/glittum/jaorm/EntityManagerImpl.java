package org.glittum.jaorm;

import static org.glittum.jaorm.internal.EntityExtractionService.findColumnToField;
import static org.glittum.jaorm.internal.EntityExtractionService.findIdColumnFromEntity;
import static org.glittum.jaorm.internal.EntityExtractionService.findIdFieldFromEntity;
import static org.glittum.jaorm.internal.EntityExtractionService.findTableFromEntity;
import static org.glittum.jaorm.internal.EntityExtractionService.generateExecutionPlan;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.criteria.EntityQueryBuilder;
import org.glittum.jaorm.criteria.FilterCriteria;
import org.glittum.jaorm.criteria.Query;
import org.glittum.jaorm.criteria.QueryBuilder;
import org.glittum.jaorm.exception.EmptyResultException;
import org.glittum.jaorm.exception.EntityInstansiationException;
import org.glittum.jaorm.exception.NonUniqueResultsSetException;
import org.glittum.jaorm.internal.ChildRecordClasses;
import org.glittum.jaorm.internal.QueryExecutionPlan;
import org.glittum.jaorm.mapper.EntityValueMapper;
import org.glittum.jaorm.mapper.FieldWithAlias;

public class EntityManagerImpl implements EntityManager {

    private static final long DEFAULT_OFFSET = 1L;
    private static final Long DEFAULT_LIMIT = 50L;
    private static final String COUNT_ANTALL = "antall";

    private final Database database;
    private final EntityValueMapper entityValueMapper = new EntityValueMapper();

    EntityManagerImpl(String jndiKey) {
        this.database = new Database(jndiKey);
    }

    EntityManagerImpl(Database database) {
        this.database = database;
    }

    @Override
    public <T> Optional<T> find(Class<T> entity, Long id) {
        String idColumn = findIdColumnFromEntity(entity);
        QueryExecutionPlan<T> plan = generateExecutionPlan(entity);

        Query<T> rootQuery = plan.getRootQuery();
        return database.queryForSingle(rootQuery.toStatement() +
                        " WHERE " + idColumn + " = ?", id,
                it -> toEntity(it, plan));
    }

    @Override
    public <T> Optional<T> findBy(QueryBuilder<T> typedQuery) {
        return findByInternal(typedQuery, null);
    }

    private <T> Optional<T> findByInternal(QueryBuilder<T> typedQuery, Class<?> rootEntity) {
        QueryExecutionPlan<T> plan = generateExecutionPlan(typedQuery, rootEntity);
        Query<T> query = plan.getRootQuery();
        List<T> ts = database.queryForList(query.toStatement(),
                it -> toEntity(it, plan), query.getParameters().toArray());
        if (ts.size() > 1) {
            throw new NonUniqueResultsSetException(query);
        }
        return ts.stream().findFirst();
    }

    @Override
    public <T> List<T> findAll(Class<T> entity) {
        QueryExecutionPlan<T> plan = generateExecutionPlan(entity);

        Query<T> rootQuery = plan.getRootQuery();
        return database.queryForList(rootQuery.toStatement(),
                it -> toEntity(it, plan));
    }

    @Override
    public <T> List<T> findAllBy(QueryBuilder<T> typedQuery) {
        QueryExecutionPlan<T> plan = generateExecutionPlan(typedQuery);
        Query<T> query = plan.getRootQuery();
        return database.queryForList(query.toStatement(),
                it -> toEntity(it, plan), query.getParameters().toArray());
    }

    @Override
    public <T> List<T> findSubsett(Class<T> entity, Long offset, Long limit) {
        QueryExecutionPlan<T> plan = generateExecutionPlan(entity);

        if (limit == null) {
            limit = DEFAULT_LIMIT;
        }
        if (offset == null) {
            offset = DEFAULT_OFFSET;
        }

        Query<T> query = plan.getRootQuery();
        return database.queryForList(query.toStatement() +
                        " LIMIT ?" +
                        " OFFSET ?",
                it -> toEntity(it, plan), offset, limit);
    }

    @Override
    public <T> Long count(Class<T> entity) {
        String entityTable = findTableFromEntity(entity);
        return Long.valueOf(database.queryForSingle("SELECT count(1) AS " + COUNT_ANTALL +
                        " FROM " + entityTable,
                this::toCount)
                .orElseThrow());
    }

    @Override
    public <T> Long countBy(QueryBuilder<T> queryBuilder) {
        Query<T> query = queryBuilder.build();
        String entityTable = findTableFromEntity(query.getEntity());
        return Long.valueOf(database.queryForSingle("SELECT count(1) AS " + COUNT_ANTALL +
                        " FROM " + entityTable + " " + query.getWhereClause(),
                query.getParameters(),
                this::toCount)
                .orElseThrow());
    }

    Database getDatabase() {
        return database;
    }

    @Override
    public <T> Long persist(T entity) {
        Field idField = findIdFieldFromEntity(entity.getClass());
        idField.setAccessible(true);
        Object id;
        try {
            id = idField.get(entity);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Unable to find id field");
        }
        if (id == null) {
            Query<?> query = EntityQueryBuilder.forClass(entity.getClass()).insert(entity).build();
            long newId = database.insert(query.toStatement(), query.getParameters().toArray(new Object[0]));
            try {
                idField.set(entity, newId);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }
            return newId;
        } else {
            Query<?> query = EntityQueryBuilder.forClass(entity.getClass()).update(entity).build();
            database.executeOperation(query.toStatement(), query.getParameters().toArray(new Object[0]));
            return (Long) id;
        }
    }

    @Override
    public <T> boolean delete(T entity) {
        String table = findTableFromEntity(entity.getClass());
        Field idField = findIdFieldFromEntity(entity.getClass());
        idField.setAccessible(true);
        String idColumn = idField.getAnnotation(Column.class).value();
        String idVariableName = table + "." + idColumn;
        try {
            Query query = EntityQueryBuilder.forClass(entity.getClass())
                    .where(FilterCriteria.equals(idColumn, idVariableName))
                    .withParameterValue(idVariableName, idField.get(entity))
                    .build();
            database.executeOperation(query.toStatement(), query.getParameters());
            return true;
        } catch (IllegalAccessException e) {
            // Should never happen.
            return false;
        }
    }

    private Integer toCount(Database.Row rs) throws SQLException {
        return rs.getInt(COUNT_ANTALL);
    }

    private <T> T toEntity(Database.Row rs, QueryExecutionPlan<T> plan) {
        Query<T> query = plan.getRootQuery();
        Class<T> entity = query.getEntity();
        try {

            if (query.hasCustomRowMapper()) {
                Database.RowMapper<T> customRowMapper = query.getCustomRowMapper();
                return customRowMapper.run(rs);
            } else {
                Map<Field, FieldWithAlias> columnToField = findColumnToField(entity);
                Constructor<T> constructor = entity.getDeclaredConstructor();
                constructor.setAccessible(true);
                T entityObject = constructor.newInstance();

                mapColumnFields(entity, rs, columnToField, entityObject);

                if (plan.requiresFetchForChildren()) {
                    mapChildRecords(entity, plan, entityObject);
                }

                return entityObject;
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SQLException e) {
            throw new EntityInstansiationException(e, entity);
        }
    }

    private <T> void mapColumnFields(Class<T> entity, Database.Row rs, Map<Field, FieldWithAlias> columnToField, T entityObject) throws IllegalAccessException {
        for (Map.Entry<Field, FieldWithAlias> entry : columnToField.entrySet()) {
            Field key = entry.getKey();
            key.setAccessible(true);
            key.set(entityObject, extractValue(entity, rs, entry));
        }
    }

    private <T> void mapChildRecords(Class<T> entity, QueryExecutionPlan<T> plan, T entityObject) throws IllegalAccessException {
        Map<ChildRecordClasses, QueryBuilder<?>> childRecordQuerries = plan.getChildRecordQuerries();
        for (ChildRecordClasses childRecordClass : childRecordQuerries.keySet()) {
            Field idField = findIdFieldFromEntity(childRecordClass.getEntity());
            Object value = idField.get(entityObject);
            String variableName = childRecordClass.getVariableName();

            QueryBuilder<?> queryBuilder = childRecordQuerries.get(childRecordClass)
                    .withParameterValue(variableName, value);

            Field entityField = childRecordClass.getEntityField();
            if (entityField.isAnnotationPresent(OneToMany.class)) {

                List<?> allBy = findAllBy(queryBuilder);
                allBy.forEach(it -> mapChildClassReference(entity, entityObject, childRecordClass, it));

                entityField.setAccessible(true);
                entityField.set(entityObject, allBy);
            }
            if (entityField.isAnnotationPresent(OneToOne.class)) {
                OneToOne oneToOne = entityField.getAnnotation(OneToOne.class);
                Optional<?> by = findByInternal(queryBuilder, entityObject.getClass());
                if (oneToOne.optional() && by.isEmpty()) {
                    throw new EmptyResultException(queryBuilder.build());
                }
                by.ifPresent(it -> mapChildClassReference(entity, entityObject, childRecordClass, it));

                entityField.setAccessible(true);
                entityField.set(entityObject, by.orElse(null));
            }
        }
    }

    private <T> void mapChildClassReference(Class<T> entity, T entityObject, ChildRecordClasses childRecordClass, Object it) {
        Field targetField = childRecordClass.getTargetField();
        targetField.setAccessible(true);
        try {
            targetField.set(it, entityObject);
        } catch (IllegalAccessException e) {
            throw new EntityInstansiationException(e, entity);
        }
    }

    private <T> Object extractValue(Class<T> entity, Database.Row rs, Map.Entry<Field, FieldWithAlias> entry) {
        if (entry.getKey().isAnnotationPresent(Column.class)) {
            return entityValueMapper.extractValueForColumn(entry, rs, entity);
        } else if (entry.getKey().isAnnotationPresent(OneToOne.class)) {
            Constructor<?> constructor;
            try {
                constructor = entry.getKey().getType().getDeclaredConstructor();
                constructor.setAccessible(true);
                return constructor.newInstance();
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        } else if (entry.getKey().isAnnotationPresent(OneToMany.class) || entry.getKey().isAnnotationPresent(ManyToOne.class)) {
            return null;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
