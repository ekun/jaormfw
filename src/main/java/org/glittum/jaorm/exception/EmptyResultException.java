package org.glittum.jaorm.exception;

import org.glittum.jaorm.criteria.Query;

public class EmptyResultException extends RuntimeException {

    public <T> EmptyResultException(Query<T> query) {
        super("Required entity is missing for '" + query + "'");
    }

}
