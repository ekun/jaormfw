package org.glittum.jaorm.exception;

public class IncompleteQueryException extends RuntimeException {

    public <T> IncompleteQueryException(Class<T> entity) {
        super("Missing parameter in query for '" + entity.getSimpleName() + "'");
    }

}
