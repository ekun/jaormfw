package org.glittum.jaorm.exception;

public class UnknownFieldOnTargetEntityException extends RuntimeException {
    public <T> UnknownFieldOnTargetEntityException(Class<?> type, String field, Class<T> entity) {
        super("Unsupported type '" + type.getSimpleName() + "' on field '" + field + "' for entity '" + entity.getSimpleName() + "'");
    }
}
