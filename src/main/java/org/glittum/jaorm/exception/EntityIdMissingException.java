package org.glittum.jaorm.exception;

public class EntityIdMissingException extends RuntimeException {

    public <T> EntityIdMissingException(Class<T> entity) {
        super("Unable to find column with @Id annotation for entity '" + entity.getSimpleName() + "'");
    }

}
