package org.glittum.jaorm.exception;

import java.util.List;
import java.util.Map;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class QueryInconsistencyException extends RuntimeException {

    public <T> QueryInconsistencyException(Map<Operation, List<Criteria>> priorityListMap, Class<T> entity) {
        super("Query is inconsistent, found multiple types of operations " + priorityListMap + " when building Query for entity '" + entity.getSimpleName() + "'");
    }
}
