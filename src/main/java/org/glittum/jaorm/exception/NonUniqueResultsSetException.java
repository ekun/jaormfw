package org.glittum.jaorm.exception;

import org.glittum.jaorm.criteria.Query;

public class NonUniqueResultsSetException extends RuntimeException {

    public <T> NonUniqueResultsSetException(Query<T> query) {
        super("Unable to extract unique respons for '" + query + "'");
    }

}
