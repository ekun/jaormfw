package org.glittum.jaorm.exception;

public class UnknownQueryVariableExeption extends RuntimeException {

    public <T> UnknownQueryVariableExeption(Class<T> entity, String parameter) {
        super("Unknown parameter for query for entity " + entity.getSimpleName() + "', parameter='" + parameter + "'");
    }

}
