package org.glittum.jaorm.exception;

public class EntityInstansiationException extends RuntimeException {
    public <T> EntityInstansiationException(Exception e, Class<T> entity) {
        super("Failed to instansiant entity '" + entity.getSimpleName() + "'", e);
    }
}
