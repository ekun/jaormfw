package org.glittum.jaorm.exception;

public class UnsupportedFieldTypeException extends RuntimeException {
    public <T> UnsupportedFieldTypeException(Class<?> type, String field, Class<T> entity) {
        super("Unsupported type '" + type.getSimpleName() + "' on field '" + field + "' for entity '" + entity.getSimpleName() + "'");
    }
}
