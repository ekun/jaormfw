package org.glittum.jaorm;

import java.util.List;
import java.util.Optional;

import org.glittum.jaorm.criteria.QueryBuilder;

public interface EntityManager {
    <T> Optional<T> find(Class<T> entity, Long id);

    <T> Optional<T> findBy(QueryBuilder<T> typedQuery);

    <T> List<T> findAll(Class<T> entity);

    <T> List<T> findAllBy(QueryBuilder<T> typedQuery);

    <T> List<T> findSubsett(Class<T> entity, Long offset, Long limit);

    <T> Long count(Class<T> entity);

    <T> Long countBy(QueryBuilder<T> queryBuilder);

    <T> Long persist(T entity);

    <T> boolean delete(T entity);

}
