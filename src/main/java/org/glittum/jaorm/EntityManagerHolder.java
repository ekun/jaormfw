package org.glittum.jaorm;

import java.util.Objects;

public class EntityManagerHolder {
    private static EntityManagerHolder ourInstance = new EntityManagerHolder();

    private EntityManager entityManager;

    private EntityManagerHolder() {
    }

    public static EntityManager get() {
        Objects.requireNonNull(ourInstance.entityManager, "Missing entity manager, did you forget to create it?");
        return ourInstance.entityManager;
    }

    public static void create(String jndiKey) {
        ourInstance.entityManager = new EntityManagerImpl(jndiKey);
    }

    public static void create(Database database) {
        ourInstance.entityManager = new EntityManagerImpl(database);
    }

}
