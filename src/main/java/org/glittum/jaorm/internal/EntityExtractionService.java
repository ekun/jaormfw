package org.glittum.jaorm.internal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.glittum.jaorm.annotation.Column;
import org.glittum.jaorm.annotation.Id;
import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.annotation.Table;
import org.glittum.jaorm.criteria.QueryBuilder;
import org.glittum.jaorm.exception.EntityIdMissingException;
import org.glittum.jaorm.mapper.FieldWithAlias;

public final class EntityExtractionService {

    private EntityExtractionService() {
    }

    public static <T> String generateSelectFieldStatement(Class<T> entity) {
        Map<Field, FieldWithAlias> columnToField = findColumnToField(entity);
        List<FieldWithAlias> values = new ArrayList<>(columnToField.values());
        return generateSelectFieldStatement(values);
    }

    public static <T> QueryExecutionPlan<T> generateExecutionPlan(Class<T> entity) {
        Map<Field, FieldWithAlias> columnToField = findColumnToField(entity);
        return new QueryExecutionPlan<>(entity, columnToField);
    }

    public static <T> QueryExecutionPlan<T> generateExecutionPlan(QueryBuilder<T> typedQuery) {
        return new QueryExecutionPlan<>(typedQuery);
    }

    public static <T> QueryExecutionPlan<T> generateExecutionPlan(QueryBuilder<T> typedQuery, Class<?> rootEntity) {
        // TODO: Guard mot evig løkke i OneToOne
        QueryExecutionPlan<T> plan = new QueryExecutionPlan<>(typedQuery);
        plan.twoWayBindingGuard(rootEntity);
        return plan;
    }

    private static String generateSelectFieldStatement(List<FieldWithAlias> values) {
        values = values.stream().filter(FieldWithAlias::isColumn).sorted().collect(Collectors.toList());
        Iterator<FieldWithAlias> iterator = values.iterator();
        StringBuilder selectBuilder = new StringBuilder(iterator.next().getSelectString());
        iterator.forEachRemaining(fieldWithAlias -> selectBuilder.append(", ").append(fieldWithAlias.getSelectString()));
        return selectBuilder.toString();
    }

    public static <T> String findIdColumnFromEntity(Class<T> entity) {
        List<Field> idFields = Arrays.stream(entity.getDeclaredFields()).filter(f -> f.isAnnotationPresent(Id.class)).collect(Collectors.toList());
        if (idFields.isEmpty()) {
            throw new EntityIdMissingException(entity);
        }
        return idFields.get(0).getAnnotation(Column.class).value(); // TODO: Support multiple columns
    }

    public static <T> Field findIdFieldFromEntity(Class<T> entity) {
        List<Field> idFields = Arrays.stream(entity.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(Id.class))
                .peek(it -> it.setAccessible(true))
                .collect(Collectors.toList());
        if (idFields.isEmpty()) {
            throw new EntityIdMissingException(entity);
        }
        Field field = idFields.get(0);
        field.setAccessible(true);
        return field; // TODO: Support multiple columns
    }

    public static <T> String findTableFromEntity(Class<T> entity) {
        return entity.getAnnotation(Table.class).value();
    }

    public static <T> Map<Field, FieldWithAlias> findColumnToField(Class<T> entity) {
        List<Field> fields = Arrays.stream(entity.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(Column.class)
                        || f.isAnnotationPresent(ManyToOne.class)
                        || f.isAnnotationPresent(OneToOne.class)
                        || f.isAnnotationPresent(OneToMany.class))
                .collect(Collectors.toList());

        Map<Field, FieldWithAlias> columnToField = new HashMap<>();
        fields.forEach(field -> {
            if (field.isAnnotationPresent(Column.class)) {
                Column fieldAnnotation = field.getAnnotation(Column.class);
                columnToField.put(field, new FieldWithAlias(fieldAnnotation, findTableFromEntity(entity)));
            } else if (field.isAnnotationPresent(ManyToOne.class)) {
                ManyToOne fieldAnnotation = field.getAnnotation(ManyToOne.class);
                columnToField.put(field, new FieldWithAlias(fieldAnnotation, findTableFromEntity(entity)));
            } else if (field.isAnnotationPresent(OneToMany.class)) {
                OneToMany fieldAnnotation = field.getAnnotation(OneToMany.class);
                columnToField.put(field, new FieldWithAlias(fieldAnnotation, findTableFromEntity(entity)));
            } else if (field.isAnnotationPresent(OneToOne.class)) {
                OneToOne fieldAnnotation = field.getAnnotation(OneToOne.class);
                columnToField.put(field, new FieldWithAlias(fieldAnnotation, findTableFromEntity(entity)));
            }
        });

        return columnToField;
    }

    public static <T> Map<Field, Object> findFieldsAndValues(T entity) {
        List<Field> fields = Arrays.stream(entity.getClass().getDeclaredFields())
                .peek(it -> it.setAccessible(true))
                .filter(f -> f.isAnnotationPresent(org.glittum.jaorm.annotation.Column.class)
                        || f.isAnnotationPresent(org.glittum.jaorm.annotation.ManyToOne.class)
                        || f.isAnnotationPresent(org.glittum.jaorm.annotation.OneToOne.class))
                .collect(Collectors.toList());

        Map<Field, Object> fieldMap = new HashMap<>();
        for (Field field : fields) {
            try {
                Object value = field.get(entity);
                if (value != null) {
                    if (field.isAnnotationPresent(org.glittum.jaorm.annotation.ManyToOne.class)
                            || field.isAnnotationPresent(org.glittum.jaorm.annotation.OneToOne.class)) {
                        Field idFieldFromEntity = findIdFieldFromEntity(value.getClass());
                        idFieldFromEntity.setAccessible(true);
                        fieldMap.put(field, idFieldFromEntity.get(value));
                    } else {
                        fieldMap.put(field, value);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return fieldMap;
    }
}