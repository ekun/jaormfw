package org.glittum.jaorm.internal;

import static org.glittum.jaorm.internal.EntityExtractionService.findColumnToField;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.glittum.jaorm.annotation.ManyToOne;
import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.criteria.EntityQueryBuilder;
import org.glittum.jaorm.criteria.FilterCriteria;
import org.glittum.jaorm.criteria.Query;
import org.glittum.jaorm.criteria.QueryBuilder;
import org.glittum.jaorm.mapper.FieldWithAlias;

/**
 * Holds the querries in order that is needed for fetching the entities.
 */
public class QueryExecutionPlan<T> {
    private final Class<T> entity;
    private final Query<T> rootQuery;
    private final Map<ChildRecordClasses, QueryBuilder<?>> childRecordQuerries = new HashMap<>();

    public QueryExecutionPlan(QueryBuilder<T> queryBuilder) {
        Objects.requireNonNull(queryBuilder, "queryBuilder");
        this.rootQuery = queryBuilder.build();
        this.entity = rootQuery.getEntity();
        // TODO: Dette bør kanskje gjøres i queryBuilder?
        generateChildRecordQuerries(findColumnToField(entity));
    }

    public QueryExecutionPlan(Class<T> entity) {
        this(entity, findColumnToField(entity));
    }

    public QueryExecutionPlan(Class<T> entity, Map<Field, FieldWithAlias> columnToField) {
        Objects.requireNonNull(entity, "entity");
        Objects.requireNonNull(columnToField, "columnToField");
        this.entity = entity;
        this.rootQuery = EntityQueryBuilder.forClass(entity).build();
        generateChildRecordQuerries(columnToField);
    }

    private void generateChildRecordQuerries(Map<Field, FieldWithAlias> columnToField) {
        Map<ChildRecordClasses, List<Field>> collect = columnToField.keySet().stream()
                .filter(field -> field.isAnnotationPresent(OneToMany.class)
                        || field.isAnnotationPresent(OneToOne.class))
                .collect(Collectors.groupingBy(ChildRecordClasses::new));

        for (ChildRecordClasses childRecordClass : collect.keySet()) {
            String column = null;
            Field targetField = childRecordClass.getTargetField();
            if (targetField.isAnnotationPresent(ManyToOne.class)) {
                column = targetField.getAnnotation(ManyToOne.class).column();
            } else if (targetField.isAnnotationPresent(OneToOne.class)) {
                column = targetField.getAnnotation(OneToOne.class).column();
            }

            QueryBuilder<?> queryBuilder = EntityQueryBuilder.forClass(childRecordClass.getTarget())
                    .where(FilterCriteria.equals(column, childRecordClass.getVariableName()));
            childRecordQuerries.put(childRecordClass, queryBuilder);
        }
    }

    public Class<T> getEntity() {
        return entity;
    }

    public boolean requiresFetchForChildren() {
        return !childRecordQuerries.isEmpty();
    }

    public Query<T> getRootQuery() {
        return rootQuery;
    }

    public Map<ChildRecordClasses, QueryBuilder<?>> getChildRecordQuerries() {
        return childRecordQuerries;
    }

    void twoWayBindingGuard(Class<?> rootEntity) {
        List<ChildRecordClasses> collect = childRecordQuerries.keySet().stream()
                .filter(cr -> cr.getEntity().equals(entity) && cr.getTarget().equals(rootEntity))
                .collect(Collectors.toList());
        collect.forEach(childRecordQuerries::remove);
    }
}
