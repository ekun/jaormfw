package org.glittum.jaorm.internal;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

import org.glittum.jaorm.annotation.OneToMany;
import org.glittum.jaorm.annotation.OneToOne;
import org.glittum.jaorm.exception.UnknownFieldOnTargetEntityException;

public class ChildRecordClasses {
    private Class<?> entity;
    private Field entityField;
    private Class<?> target;
    private Field targetField;

    ChildRecordClasses(Field field) {
        this.entity = field.getDeclaringClass();
        this.entityField = field;
        this.target = getTargetClass(field);
        this.targetField = extractTargetField(field, target);
    }

    private Class<?> getTargetClass(Field field) {
        if (Collection.class.isAssignableFrom(field.getType())) {
            return (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
        }
        return field.getType();
    }

    private Field extractTargetField(Field field, Class<?> target) {
        if (field.isAnnotationPresent(OneToMany.class)) {
            OneToMany oneToMany = field.getAnnotation(OneToMany.class);
            String mappedBy = oneToMany.mappedBy();
            try {
                return target.getDeclaredField(mappedBy);
            } catch (NoSuchFieldException e) {
                throw new UnknownFieldOnTargetEntityException(target, mappedBy, entity);
            }
        }
        if (field.isAnnotationPresent(OneToOne.class)) {
            OneToOne oneToMany = field.getAnnotation(OneToOne.class);
            try {
                return Arrays.stream(target.getDeclaredFields())
                        .filter(f -> f.isAnnotationPresent(OneToOne.class) && f.getType().equals(entity))
                        .findFirst()
                        .orElseThrow(NoSuchFieldException::new);
            } catch (NoSuchFieldException e) {
                throw new UnknownFieldOnTargetEntityException(target, oneToMany.column(), entity);
            }
        }
        throw new UnknownFieldOnTargetEntityException(target, null, entity);
    }

    public Class<?> getEntity() {
        return entity;
    }

    public Field getEntityField() {
        return entityField;
    }

    public Class<?> getTarget() {
        return target;
    }

    public Field getTargetField() {
        return targetField;
    }

    public String getVariableName() {
        return target.getSimpleName() + ".id";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChildRecordClasses that = (ChildRecordClasses) o;
        return Objects.equals(entity, that.entity) &&
                Objects.equals(entityField, that.entityField) &&
                Objects.equals(target, that.target) &&
                Objects.equals(targetField, that.targetField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity, entityField, target, targetField);
    }

    @Override
    public String toString() {
        return "ChildRecordClasses{" +
                "entity=" + entity +
                ", entityField=" + entityField +
                ", target=" + target +
                ", targetField=" + targetField +
                '}';
    }
}
