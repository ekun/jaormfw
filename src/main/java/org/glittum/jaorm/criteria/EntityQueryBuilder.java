package org.glittum.jaorm.criteria;

public class EntityQueryBuilder<T> extends QueryBuilder<T> {

    private EntityQueryBuilder(Class<T> entity) {
        super(entity);
    }

    public static <T> EntityQueryBuilder<T> forClass(Class<T> entity) {
        return new EntityQueryBuilder<>(entity);
    }

}
