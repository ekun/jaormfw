package org.glittum.jaorm.criteria;

public enum Operator {
    GREATER(">"),
    GREATER_OR_EQUAL(">="),
    EQUALS("="),
    NOT_EQUALS("!="),
    LESS("<"),
    LESS_OR_EQUAL("<="),
    IN("IN"),
    LIKE("LIKE");

    private String operator;

    Operator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }
}
