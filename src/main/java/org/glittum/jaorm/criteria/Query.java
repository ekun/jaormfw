package org.glittum.jaorm.criteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.glittum.jaorm.Database;
import org.glittum.jaorm.criteria.internal.DefaultEntityFromCriteria;
import org.glittum.jaorm.criteria.internal.DefaultEntitySelectCriteria;
import org.glittum.jaorm.criteria.internal.Insert;
import org.glittum.jaorm.criteria.internal.ReturnGeneratedId;
import org.glittum.jaorm.exception.IncompleteQueryException;
import org.glittum.jaorm.exception.QueryInconsistencyException;

public class Query<T> {

    private final String statement;
    private final Class<T> entity;
    private final List<Object> filterParameters = new ArrayList<>();
    private final List<String> filterVariables = new ArrayList<>();
    private Database.RowMapper<T> customRowMapper;

    Query(Class<T> entity, Set<Criteria> criterias) {
        this.entity = entity;
        Map<Operation, List<Criteria>> priorityListMap = criterias.stream()
                .collect(Collectors.groupingBy(Criteria::getPriority));
        // DO consistency check
        priorityListMap = checkAndAddDefaults(priorityListMap);

        statement = buildStatement(priorityListMap);
    }

    private Map<Operation, List<Criteria>> checkAndAddDefaults(Map<Operation, List<Criteria>> priorityListMap) {
        HashMap<Operation, List<Criteria>> map = new HashMap<>(priorityListMap);
        checkIfStatementContainsOnlyOneCommand(priorityListMap);
        checkIfInsertStatementIsConsistent(priorityListMap);
        if (!priorityListMap.containsKey(Operation.SELECT) && !priorityListMap.containsKey(Operation.DELETE)
                && !priorityListMap.containsKey(Operation.INSERT)) {
            map.put(Operation.SELECT, List.of(new DefaultEntitySelectCriteria<>(entity)));
        }
        if (!priorityListMap.containsKey(Operation.FROM) && !priorityListMap.containsKey(Operation.INSERT)) {
            map.put(Operation.FROM, List.of(new DefaultEntityFromCriteria<>(entity)));
        }
        if (priorityListMap.containsKey(Operation.INSERT) && !priorityListMap.containsKey(Operation.RETURNING)) {
            map.put(Operation.RETURNING, List.of(new ReturnGeneratedId<>(entity)));
        }
        return map;
    }

    private void checkIfInsertStatementIsConsistent(Map<Operation, List<Criteria>> priorityListMap) {
        if (priorityListMap.containsKey(Operation.FROM) && priorityListMap.containsKey(Operation.INSERT)) {
            throw new QueryInconsistencyException(priorityListMap, entity);
        }
    }

    private void checkIfStatementContainsOnlyOneCommand(Map<Operation, List<Criteria>> priorityListMap) {
        int counter = 0;
        if (priorityListMap.containsKey(Operation.SELECT)) {
            counter++;
        }
        if (priorityListMap.containsKey(Operation.DELETE)) {
            counter++;
        }
        if (priorityListMap.containsKey(Operation.INSERT)) {
            counter++;
        }
        if (counter > 1) {
            throw new QueryInconsistencyException(priorityListMap, entity);
        }
    }

    private String buildStatement(Map<Operation, List<Criteria>> map) {
        final StringBuilder query = new StringBuilder();
        map.keySet()
                .stream()
                .sorted(Comparator.comparingInt(Operation::priority))
                .forEachOrdered(key -> {
                    query.append(key.name()).append(" ");
                    List<Criteria> criteria = map.get(key).stream().sorted(Criteria::compareTo).collect(Collectors.toList());
                    if (!criteria.isEmpty()) {
                        Iterator<Criteria> iterator = criteria.iterator();
                        query.append(parseCriteria(iterator));
                        while (iterator.hasNext()) {
                            query.append(key.getSeperator())
                                    .append(parseCriteria(iterator));
                        }
                        query.append(key.getKeySeperator());
                    }
                });
        String s = query.toString();
        return s.substring(0, s.length() - 1); // Fjerner siste space
    }

    private String parseCriteria(Iterator<Criteria> iterator) {
        Criteria next = iterator.next();
        if (next instanceof AndCriteria) {
            AndCriteria and = (AndCriteria) next;
            extractVariableFrom(and.crit1());
            extractVariableFrom(and.crit2());
        } else if (next instanceof OrCriteria) {
            OrCriteria or = (OrCriteria) next;
            extractVariableFrom(or.crit1());
            extractVariableFrom(or.crit2());
        } else if (next instanceof FilterCriteria) {
            extractVariableFrom((FilterCriteria) next);
        } else if (next instanceof Insert) {
            extractVariableFrom((Insert) next);
        }
        return next.toStatementPart();
    }

    private void extractVariableFrom(Insert insert) {
        for (String column : insert.getColumns()) {
            if (column != null) {
                filterVariables.add(column);
            }
        }
    }

    private void extractVariableFrom(FilterCriteria next) {
        String valueVariable = next.getValueVariable();
        if (valueVariable != null) {
            filterVariables.add(valueVariable);
        }
    }

    public String getWhereClause() {
        Optional<Operation> min = Arrays.stream(Operation.values())
                .filter(p -> p.priority() > Operation.WHERE.priority())
                .filter(p -> statement.contains(p.name()))
                .min(Comparator.comparingInt(Operation::priority));
        if (min.isPresent()) {
            Operation operation = min.get();
            return statement.substring(statement.indexOf(" WHERE "), statement.indexOf(operation.name()));
        }
        return statement.substring(statement.indexOf(" WHERE "));
    }

    public String toStatement() {
        return statement;
    }

    public Class<T> getEntity() {
        return entity;
    }


    public List<Object> getParameters() {
        return filterParameters;
    }

    void setParameters(Map<String, Object> variablesMap) {
        if (filterVariables.size() != variablesMap.keySet().size()) {
            throw new IncompleteQueryException(entity);
        }
        for (String filterVariable : filterVariables) {
            filterParameters.add(variablesMap.get(filterVariable));
        }
    }

    public Database.RowMapper<T> getCustomRowMapper() {
        return customRowMapper;
    }

    void setCustomRowMapper(Database.RowMapper<T> customRowMapper) {
        this.customRowMapper = customRowMapper;
    }

    public boolean hasCustomRowMapper() {
        return customRowMapper != null;
    }

    @Override
    public String toString() {
        return "Query{" +
                "statement='" + statement + '\'' +
                ", entity=" + entity +
                '}';
    }
}
