package org.glittum.jaorm.criteria;

public interface Criteria extends Comparable {

    String toStatementPart();

    Operation getPriority();

    @Override
    default int compareTo(Object o) {
        Criteria o1 = (Criteria) o;
        if (getPriority() != o1.getPriority()) {
            return Integer.compare(getPriority().priority(), o1.getPriority().priority());
        }
        return toStatementPart().compareTo(o1.toStatementPart());
    }

}
