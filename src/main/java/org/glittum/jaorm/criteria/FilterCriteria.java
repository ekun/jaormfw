package org.glittum.jaorm.criteria;

import java.util.Objects;

public class FilterCriteria implements Criteria {

    private String field;
    private Operator operator;
    private String valueVariable;

    public FilterCriteria() {

    }

    FilterCriteria(String field, Operator operator, String valueVariable) {
        Objects.requireNonNull(field, "field");
        Objects.requireNonNull(operator, "operator");
        Objects.requireNonNull(valueVariable, "valueVariable");

        this.field = field;
        this.operator = operator;
        this.valueVariable = valueVariable;
    }

    public static FilterCriteria equals(String field, String variableName) {
        return new FilterCriteria(field, Operator.EQUALS, variableName);
    }

    public static FilterCriteria not(String field, String variableName) {
        return new FilterCriteria(field, Operator.NOT_EQUALS, variableName);
    }

    public static FilterCriteria like(String field, String variableName) {
        return new FilterCriteria(field, Operator.LIKE, variableName);
    }

    public static FilterCriteria greater(String field, String variableName) {
        return new FilterCriteria(field, Operator.GREATER, variableName);
    }

    public static FilterCriteria greaterOrEqual(String field, String variableName) {
        return new FilterCriteria(field, Operator.GREATER_OR_EQUAL, variableName);
    }

    public static FilterCriteria less(String field, String variableName) {
        return new FilterCriteria(field, Operator.LESS, variableName);
    }

    public static FilterCriteria lessOrEqual(String field, String variableName) {
        return new FilterCriteria(field, Operator.LESS_OR_EQUAL, variableName);
    }

    public static FilterCriteria in(String field, String variableName) {
        return new FilterCriteria(field, Operator.IN, variableName);
    }

    @Override
    public String toStatementPart() {
        Objects.requireNonNull(field, "field");
        Objects.requireNonNull(operator, "operator");
        Objects.requireNonNull(valueVariable, "valueVariable");

        if (Operator.IN.equals(operator)) {
            return field + " " + operator.getOperator() + " (?)";
        }
        return field + " " + operator.getOperator() + " ?";
    }

    public String getValueVariable() {
        return valueVariable;
    }

    public OrCriteria or(FilterCriteria filter) {
        return new OrCriteria(this, filter);
    }

    public AndCriteria and(FilterCriteria filter) {
        return new AndCriteria(this, filter);
    }

    @Override
    public Operation getPriority() {
        return Operation.WHERE;
    }

}
