package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class Offset implements Criteria {

    private final long offset;

    public Offset(long offset) {
        this.offset = offset;
    }

    @Override
    public String toStatementPart() {
        return Long.valueOf(offset).toString();
    }

    @Override
    public Operation getPriority() {
        return Operation.OFFSET;
    }
}
