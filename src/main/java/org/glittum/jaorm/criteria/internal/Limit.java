package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class Limit implements Criteria {

    private final long limit;

    public Limit(long limit) {
        this.limit = limit;
    }

    @Override
    public String toStatementPart() {
        return Long.valueOf(limit).toString();
    }

    @Override
    public Operation getPriority() {
        return Operation.LIMIT;
    }
}
