package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;
import org.glittum.jaorm.internal.EntityExtractionService;

public class ReturnGeneratedId<T> implements Criteria {
    private Class<T> entity;

    public ReturnGeneratedId(Class<T> entity) {
        this.entity = entity;
    }

    @Override
    public String toStatementPart() {
        return EntityExtractionService.findIdColumnFromEntity(entity) + " AS id";
    }

    @Override
    public Operation getPriority() {
        return Operation.RETURNING;
    }
}
