package org.glittum.jaorm.criteria.internal;

import static org.glittum.jaorm.internal.EntityExtractionService.generateSelectFieldStatement;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class DefaultEntitySelectCriteria<T> implements Criteria {
    private Class<T> entity;

    public DefaultEntitySelectCriteria(Class<T> entity) {
        this.entity = entity;
    }

    @Override
    public String toStatementPart() {
        return generateSelectFieldStatement(entity);
    }

    @Override
    public Operation getPriority() {
        return Operation.SELECT;
    }
}
