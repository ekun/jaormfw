package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Direction;
import org.glittum.jaorm.criteria.Operation;

public class Order implements Criteria {

    private final String field;
    private final Direction direction;

    public Order(String field, Direction direction) {
        this.field = field;
        this.direction = direction;
    }

    @Override
    public String toStatementPart() {
        return "BY " + field + " " + direction.name();
    }

    @Override
    public Operation getPriority() {
        return Operation.ORDER;
    }
}
