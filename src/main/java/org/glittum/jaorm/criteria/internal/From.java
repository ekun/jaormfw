package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class From implements Criteria {

    private final String table;

    public From(String table) {
        this.table = table;
    }

    @Override
    public String toStatementPart() {
        return table;
    }

    @Override
    public Operation getPriority() {
        return Operation.FROM;
    }
}
