package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Aggregate;
import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class AggregatedSelect implements Criteria {

    private Aggregate aggregate;
    private final String field;

    public AggregatedSelect(Aggregate aggregate, String field) {
        this.aggregate = aggregate;
        this.field = field;
    }

    @Override
    public String toStatementPart() {
        return aggregate.name() + "(" + field + ")";
    }

    @Override
    public Operation getPriority() {
        return Operation.SELECT;
    }
}
