package org.glittum.jaorm.criteria.internal;

import static org.glittum.jaorm.internal.EntityExtractionService.findTableFromEntity;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class DefaultEntityFromCriteria<T> implements Criteria {
    private Class<T> entity;

    public DefaultEntityFromCriteria(Class<T> entity) {
        this.entity = entity;
    }

    @Override
    public String toStatementPart() {
        return findTableFromEntity(entity);
    }

    @Override
    public Operation getPriority() {
        return Operation.FROM;
    }
}
