package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class Delete implements Criteria {

    public Delete() {
    }

    @Override
    public String toStatementPart() {
        return "";
    }

    @Override
    public Operation getPriority() {
        return Operation.DELETE;
    }
}
