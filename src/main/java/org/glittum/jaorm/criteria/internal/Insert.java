package org.glittum.jaorm.criteria.internal;

import java.util.List;
import java.util.StringJoiner;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class Insert implements Criteria {

    private String table;
    private List<String> columns;

    public Insert(String table, List<String> columns) {
        this.table = table;
        this.columns = columns;
    }

    @Override
    public String toStatementPart() {
        StringJoiner columnJoiner = new StringJoiner(", ");
        StringJoiner valuesJoiner = new StringJoiner(", ");
        for (String column : columns) {
            columnJoiner.add(column);
            valuesJoiner.add("?");
        }
        return "INTO " + table + " (" + columnJoiner.toString() + ") VALUES (" + valuesJoiner.toString() + ")";
    }

    @Override
    public Operation getPriority() {
        return Operation.INSERT;
    }

    public List<String> getColumns() {
        return columns;
    }
}
