package org.glittum.jaorm.criteria.internal;

import org.glittum.jaorm.criteria.Criteria;
import org.glittum.jaorm.criteria.Operation;

public class Select implements Criteria {

    private final String field;

    public Select(String field) {
        this.field = field;
    }

    @Override
    public String toStatementPart() {
        return field;
    }

    @Override
    public Operation getPriority() {
        return Operation.SELECT;
    }
}
