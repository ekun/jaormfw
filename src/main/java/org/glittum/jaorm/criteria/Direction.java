package org.glittum.jaorm.criteria;

public enum Direction {
    DESC, ASC
}
