package org.glittum.jaorm.criteria;

import static org.glittum.jaorm.internal.EntityExtractionService.findFieldsAndValues;
import static org.glittum.jaorm.internal.EntityExtractionService.findTableFromEntity;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.glittum.jaorm.criteria.internal.Delete;
import org.glittum.jaorm.criteria.internal.From;
import org.glittum.jaorm.criteria.internal.Insert;
import org.glittum.jaorm.criteria.internal.Limit;
import org.glittum.jaorm.criteria.internal.Offset;
import org.glittum.jaorm.criteria.internal.Order;
import org.glittum.jaorm.criteria.internal.Select;

public abstract class QueryBuilder<T> {
    protected final Class<T> entity;
    private final Set<Criteria> criteras = new HashSet<>();
    private final Map<String, Object> variablesMap = new HashMap<>();

    QueryBuilder(Class<T> entity) {
        this.entity = entity;
    }

    /**
     * Fields to select from the entity
     *
     * @param fields field in the database
     * @return builder
     */
    public QueryBuilder<T> select(String... fields) {
        criteras.removeIf(it -> List.of(Operation.DELETE, Operation.INSERT).contains(it.getPriority()));
        for (String field : fields) {
            Objects.requireNonNull(field);
            criteras.add(new Select(field));
        }
        return this;
    }

    /**
     * Declairs that a query in an UPDATE-statement
     *
     * @param entityObject entity
     * @return builder
     */
    public QueryBuilder<T> update(Object entityObject) {
        throw new IllegalStateException("Not implemented yet exception");
    }

    /**
     * Declairs that a query is an INSERT-statement
     *
     * @param entityObject entity
     * @return builder
     */
    public QueryBuilder<T> insert(Object entityObject) {
        criteras.removeIf(it -> List.of(Operation.DELETE, Operation.SELECT, Operation.INSERT).contains(it.getPriority()));
        String tableFromEntity = findTableFromEntity(entity);
        Map<Field, Object> fieldsAndValues = findFieldsAndValues(entityObject);
        List<String> columns = fieldsAndValues.keySet().stream().map(this::annotationToCollum).collect(Collectors.toList());
        for (Map.Entry<Field, Object> entry : fieldsAndValues.entrySet()) {
            variablesMap.put(annotationToCollum(entry.getKey()), entry.getValue());
        }
        criteras.add(new Insert(tableFromEntity, columns));
        return this;
    }

    private String annotationToCollum(Field field) {
        if (field.isAnnotationPresent(org.glittum.jaorm.annotation.Column.class)) {
            return field.getAnnotation(org.glittum.jaorm.annotation.Column.class).value();
        }
        if (field.isAnnotationPresent(org.glittum.jaorm.annotation.OneToOne.class)) {
            return field.getAnnotation(org.glittum.jaorm.annotation.OneToOne.class).column();
        }
        if (field.isAnnotationPresent(org.glittum.jaorm.annotation.ManyToOne.class)) {
            return field.getAnnotation(org.glittum.jaorm.annotation.ManyToOne.class).column();
        }
        return null;
    }

    /**
     * Declairs that a query is an DELETE-statement
     *
     * @return builder
     */
    public QueryBuilder<T> delete() {
        criteras.removeIf(it -> List.of(Operation.SELECT, Operation.INSERT, Operation.DELETE).contains(it.getPriority()));
        criteras.add(new Delete());
        return this;
    }

    /**
     * Declairs that a query is an DELETE-statement for table
     *
     * @param table table
     * @return builder
     */
    public QueryBuilder<T> delete(String table) {
        Objects.requireNonNull(table);
        criteras.removeIf(it -> List.of(Operation.SELECT, Operation.INSERT).contains(it.getPriority()));
        criteras.add(new Delete());
        criteras.add(new From(table));
        return this;
    }

    /**
     * Define what tables to select from.
     *
     * @param tables tables
     * @return builder
     */
    public QueryBuilder<T> from(String... tables) {
        for (String table : tables) {
            Objects.requireNonNull(table, "table cannot be null");
            criteras.add(new From(table));
        }
        return this;
    }

    /**
     * Adding multiple at this leveling will be evaluated as AND
     * <p>
     * Filter for a query (the WHERE clause)
     *
     * @param filter a filter defined by filterCritera
     * @return builder
     * @see org.glittum.jaorm.criteria.FilterCriteria
     */
    public QueryBuilder<T> where(FilterCriteria filter) {
        Objects.requireNonNull(filter, "filter");
        criteras.add(filter);
        return this;
    }

    /**
     * Setting a parameter value for a FilterCriteria
     *
     * @param parameter the name of the parameter
     * @param value     the value
     * @return builder
     * @see org.glittum.jaorm.criteria.FilterCriteria
     */
    public QueryBuilder<T> withParameterValue(String parameter, Object value) {
        Objects.requireNonNull(parameter, "Parameter name must be sett");
        variablesMap.put(parameter, value);
        return this;
    }

    /**
     * Adding a criteria for ordering the respons of the query
     *
     * @param field     field to be sorted after
     * @param direction direction of the sorting, if null then default value will be {@link Direction#ASC}
     * @return builder
     */
    public QueryBuilder<T> order(String field, Direction direction) {
        Objects.requireNonNull(field, "Must declair a field");
        if (direction == null) {
            direction = Direction.ASC;
        }
        criteras.add(new Order(field, direction));
        return this;
    }

    /**
     * Setting an limit for number of rows to be returned by the list query
     *
     * @param limit limit
     * @return builder
     */
    public QueryBuilder<T> limit(long limit) {
        criteras.removeIf(it -> it instanceof Limit);
        criteras.add(new Limit(limit));
        return this;
    }

    /**
     * Setting an offset for a list query
     *
     * @param offset the offset
     * @return builder
     */
    public QueryBuilder<T> offset(long offset) {
        criteras.removeIf(it -> it instanceof Offset);
        criteras.add(new Offset(offset));
        return this;
    }

    /**
     * Builds the query to create a executable query by the EntityManager
     *
     * @return query
     * @see org.glittum.jaorm.EntityManager
     * @see org.glittum.jaorm.criteria.Query
     */
    public Query<T> build() {
        Query<T> query = new Query<>(entity, criteras);
        query.setParameters(variablesMap);
        return query;
    }
}
