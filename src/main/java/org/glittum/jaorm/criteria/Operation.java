package org.glittum.jaorm.criteria;

public enum Operation {
    SELECT(10, ", "),
    DELETE(10, "", ""),
    INSERT(10, "", " "),
    FROM(20, ", "),
    JOIN(30, " "),
    WHERE(40, " AND "),
    ORDER(50, ", "),
    GROUP(60, ", "),
    OFFSET(70, " "),
    LIMIT(80, " "),
    RETURNING(100, " ");

    private int priority;
    private String seperator;
    private String keySeperator = " ";

    Operation(int priority, String seperator) {
        this.priority = priority;
        this.seperator = seperator;
    }

    Operation(int priority, String seperator, String keySeperator) {
        this.priority = priority;
        this.seperator = seperator;
        this.keySeperator = keySeperator;
    }

    public int priority() {
        return priority;
    }

    public String getSeperator() {
        return seperator;
    }

    public String getKeySeperator() {
        return keySeperator;
    }
}
