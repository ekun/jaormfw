package org.glittum.jaorm.criteria;

class OrCriteria extends FilterCriteria {

    private FilterCriteria crit1;
    private FilterCriteria crit2;

    OrCriteria(FilterCriteria crit1, FilterCriteria crit2) {
        this.crit1 = crit1;
        this.crit2 = crit2;
    }

    FilterCriteria crit1() {
        return crit1;
    }

    FilterCriteria crit2() {
        return crit2;
    }

    @Override
    public String toStatementPart() {
        return "(" + crit1.toStatementPart() + " OR " + crit2.toStatementPart() + ")";
    }
}
