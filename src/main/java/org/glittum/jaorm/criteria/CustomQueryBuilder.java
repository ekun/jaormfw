package org.glittum.jaorm.criteria;

import java.util.Objects;

import org.glittum.jaorm.Database;

public class CustomQueryBuilder<T> extends QueryBuilder<T> {

    private Database.RowMapper<T> rowMapper;

    private CustomQueryBuilder(Class<T> entity) {
        super(entity);
    }

    public static <T> CustomQueryBuilder<T> forClass(Class<T> entity) {
        return new CustomQueryBuilder<>(entity);
    }

    /**
     * Supplie a mapper for mapping sql-response to object of class T.
     *
     * @param rowMapper rowmapper for the entity of class T.
     * @return builder
     */
    public CustomQueryBuilder<T> withMapper(Database.RowMapper<T> rowMapper) {
        this.rowMapper = rowMapper;
        return this;
    }

    @Override
    public Query<T> build() {
        Objects.requireNonNull(rowMapper, "Requires a custom RowMapper for CustomQuery.");
        Query<T> query = super.build();
        query.setCustomRowMapper(rowMapper);
        return query;
    }

}
