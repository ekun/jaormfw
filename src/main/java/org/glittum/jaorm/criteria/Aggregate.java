package org.glittum.jaorm.criteria;

public enum Aggregate {
    SUM, MIN, MAX, COUNT, AVG
}
