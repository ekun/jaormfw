package org.glittum.jaorm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OneToMany {
    /**
     * Field on the entity mapped to the FK on the referring entity.
     * @return entity-fielnd in the relation
     */
    String mappedBy();
}
