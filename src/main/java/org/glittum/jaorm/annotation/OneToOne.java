package org.glittum.jaorm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OneToOne {

    /**
     * Columns in the table referencing the other entity
     *
     * @return column in table
     */
    String column() default "";

    /**
     * Is the relation optional
     *
     * @return optional
     */
    boolean optional() default true;

}
