module jaormfw {
    requires java.sql;
    requires java.naming;

    exports org.glittum.jaorm.annotation;
    exports org.glittum.jaorm.criteria;
    exports org.glittum.jaorm;

}