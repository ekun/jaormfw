# Just Another ORM Framework

**J**ust **A**nother **ORM** **F**rame**w**ork is a library providing Object/Relational Mapping (ORM) support to applications, libraries and frameworks.
It is a very simple eager fetching entities.

Currently a WIP.

[![pipeline status](https://gitlab.com/ekun/glittum.org/badges/master/pipeline.svg)](https://gitlab.com/ekun/jaormfw/commits/master) [![coverage report](https://gitlab.com/ekun/jaormfw/badges/master/coverage.svg)](https://gitlab.com/ekun/jaormfw/commits/master)  [![Maven Central](https://img.shields.io/maven-central/v/org.glittum/jaorm.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22org.glittum%22%20AND%20a:%22jaorm%22)